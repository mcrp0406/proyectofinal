﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FinalPedidos
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Entrar : ContentPage
	{
		public Entrar ()
		{
			InitializeComponent ();
		}

        async private void crea2(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CrearCuenta());
        }
        async private void entrar1(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new menu());
        }
    }
}